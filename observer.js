function Click() {
    this.handlers = [];  // observers
}

Click.prototype = {

    subscribe: function (fn) {
        this.handlers.push(fn);
    },

    unsubscribe: function (fn) {
        this.handlers = this.handlers.filter(
            function (item) {
                if (item !== fn) {
                    return item;
                }
            }
        );
    },

    fire: function (o, thisObj) {
        var scope = thisObj ;
        this.handlers.forEach(function (item) {
            item.call(scope, o);
        });
    }
}

function run() {

    var sub1 = function (update) {
        console.log("fired: " + update);
    };

    var sub2 = function (update){
        if(update === "network update"){
            console.log("Network Update happened");
        }else{
            console.log("Network is fine");
        }
    }

    var updateSource = new Click();

    updateSource.subscribe(sub1);
    updateSource.subscribe(sub2);

    updateSource.fire("User Event")
    updateSource.fire("network update")

    updateSource.unsubscribe(sub1)
    updateSource.fire("User Event")
    updateSource.fire("User Event")
    updateSource.fire("User Event")
    updateSource.subscribe(sub1)
    updateSource.fire("Last Event")
}


run()