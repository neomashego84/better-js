function print1(){
    console.log("One");
}

let print2  = function (){
    console.log("Two");
    return print1()
}

let sayThree = function(){
    return sayFour
}

let sayFour = () => {
    return "Four"
}

let printFive = () => console.log("Five");
let saySix = () => "Six"

function execute(fn){
    fn() // Three()
    console.log(fn+" was executed");
}


myDinner = {
    side: "Fries",
    drink: "Tea",
    main: "Chicken",
    order: function (){
        console.log("Your order of "+this.main+" and "+this.side+" will be served soon");
    }
}

let bigfun = function() {
    console.log("Big Function");
    return {
        smallFun : function(){
            console.log("Small Function");
        }
    }
}

function order (){
    this.main = "Chicken"
    this.side = "Fries"
    this.drink = "Orange"
}

order.prototype = {
    total: 100,
    printOrder: function() {
        console.log("Your order contains "+this.main+"  "+ this.side+" "+this.drink+" with total price of "+this.total);
    }
}


function run(){
    let myorder = new order()
    console.log(myorder.main);
    console.log(myorder.total);
    myorder.printOrder()
}

run()