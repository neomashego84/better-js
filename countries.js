class CountryList{
    list = []
}

let Singleton = function(){
    let countriesObj
    function create(){
        countriesObj = new CountryList()
    }
    return{
        get: function(){
            if(!countriesObj){
                create()
            }
            return countriesObj
        }
    }
}()

module.exports = {Singleton}