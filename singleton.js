let Countries = require('./countries')

function run(){

    let c1 = Countries.Singleton.get()
    let c2 = Countries.Singleton.get()

    c1.list.push("ZA")
    c2.list.push("EG")

    console.log(c1.list);
    console.log(c2.list);
    // console.log(c3.list);
}

run()