class Validator{
    lengthIsBetween(str, min, max){
        return str.length >= min &&
        str.length <= max
    }
    lessThanOrEqual(str, length){
        return str.length <= length
    }
    moreThanOrEqual(str, length){
        return str.length >= length
    }
    containsChar(str, ch){
        return str.indexOf(ch) != -1
    }
    doesntContainAchar(str, ch){
        return str.indexOf(ch) == -1
    }
    isNotEmpty(str){
        return str !=null
    }
}

let Singleton = function(){
    let validatorObj
    function create(){
        validatorObj= new Validator()
    }
    return{
        get: function(){
            if(!validatorObj){
                create()
            }
            return validatorObj
        }
    }
}()


module.exports = {Singleton}