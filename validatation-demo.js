class RegisterationDataValidator{
    constructor(password, email){
        this.password = password
        this.email = email
        this.phone = "0000"
    }
    validPassword(){
        // password should be more than 8 char - less then 20 - no white spaces
        if(!this.passwordDoesntContainSpace()){
            return "Invalid Char was found"
        }
        if(!this.passwordHasValidLength()){
            return "Invalid Length"
        }
        return "OK"

    }
    passwordHasValidLength() {
        return this.isNotLong() &&
            this.isNotShort() &&
            this.isNotEmpty()
    }

    validEmail(){
        // email should be more than 5 char less then 50 - contains @ and .
        return true
    }
    isNotShort(){
        return this.password.length >= 8
    }
    isNotLong(){
        return this.password.length <= 20
    }
    isNotEmpty(){
        return this.password != null && this.password != undefined
    }
    passwordDoesntContainSpace(){
        return this.password.indexOf(" ") != -1
    }

}

function run(){
    let myValidator = new RegisterationDataValidator("1234567890","1234567890");
    let validator =new RegisterationDataValidator("1234567890","")
    // validator.password = "1234567890"
    console.log(validator.validPassword());
    // myValidator.password = "1234"
    console.log(validator.validPassword());
    console.log("Runnning ..");
}

run()